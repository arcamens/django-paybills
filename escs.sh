##############################################################################
cd ~/projects/
git clone git@bitbucket.org:arcamens/paybills.git paybills-code
##############################################################################

# push, paybills, master.
cd ~/projects/django-paybills-code
# clean up all .pyc files. 
find . -name "*.pyc" -exec rm -f {} \;

git status
git add *
git commit -a
git push -u origin master
##############################################################################
# install, paybills, dependencies, virtualenv.
cd ~/.virtualenvs/
source paybills/bin/activate
cd ~/projects/paybills-code
pip install -r requirements.txt 
##############################################################################
# delete last commit.

cd ~/projects/paybills-code
git reset HEAD^ --hard
git push -f
##############################################################################
# create, first, release, test, tag.

git tag -a 0.0.1 -m 'Initial release'
git push origin : 0.0.1
##############################################################################
git rm -r --cached ./*/migrations/
git rm --cached db.sqlite3



